# Logfmt Logger

A simple logger to log in logfmt format.
Read more about Logfmt https://brandur.org/logfmt

## Getting Startet

```
require 'logfmt_logger'

logger = Logfmt::Logger.new
logger.info(hello: 'hi', message: 'this is a nice log')
```

```
> level=INFO ts="2019-07-05 23:55:53 +0200" hello="hi message=this is a nice log"
```
