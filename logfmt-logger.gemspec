Gem::Specification.new do |s|
  s.name        = 'logfmt-logger'
  s.version     = '1.0'
  s.date        = '2019-07-05'
  s.description = "Simple logger for logfmt"
  s.summary     = "Log in logfmt format"
  s.authors     = ["Andy Soiron"]
  s.email       = 'a.soiron@gmail.com'
  s.files       = ["lib/logfmt_logger.rb"]
  s.licenses    = ["MIT"]
  s.homepage    = "https://gitlab.com/Andysoiron/logfmt-logger"
end
