# frozen_string_literal: true

require 'spec_helper'

describe 'LogfmtLogger' do
  let(:custom_logger) { Logfmt::Logger.new }

  shared_context 'logging in logfmt' do
    context 'when the message is a hash' do
      let(:message) { { do_i_like_cheese: 'yes' } }
      let(:expected_hash_result) do
        /level=#{severity} ts=\".{25}\" do_i_like_cheese=yes/
      end

      specify do
        expect { subject }.to output(expected_hash_result).to_stdout_from_any_process
      end
    end

    context 'when the message is a string' do
      let(:message) { "I like cheese" }
      let(:expected_string_result) do
        /level=#{severity} ts=\".{25}\" message=\"I like cheese\"/
      end

      specify do
        expect { subject }.to output(expected_string_result).to_stdout_from_any_process
      end
    end
  end

  describe '#info' do
    subject do
      custom_logger.info(message)
    end
    let(:severity) { 'INFO' }
    it_behaves_like 'logging in logfmt'
  end

  describe '#debug' do
    subject do
      custom_logger.debug(message)
    end
    let(:severity) { 'DEBUG' }
    it_behaves_like 'logging in logfmt'
  end

  describe '#error' do
    subject do
      custom_logger.error(message)
    end
    let(:severity) { 'ERROR' }
    it_behaves_like 'logging in logfmt'
  end

  describe '#fatal' do
    subject do
      custom_logger.fatal(message)
    end
    let(:severity) { 'FATAL' }
    it_behaves_like 'logging in logfmt'
  end

  describe '#unknown' do
    subject do
      custom_logger.unknown(message)
    end
    let(:severity) { 'ANY' }
    it_behaves_like 'logging in logfmt'
  end

  describe '#warn' do
    subject do
      custom_logger.warn(message)
    end
    let(:severity) { 'WARN' }
    it_behaves_like 'logging in logfmt'
  end
end
