# frozen_string_literal: true

require 'spec_helper'

describe 'Hash' do
  describe '#to_logfmt' do
    subject { hash.to_logfmt }

    let(:hash) { { "foo" => 'bar' } }

    shared_context 'parsable with logfmt-ruby' do
      specify do
        expect(Logfmt.parse(subject)).to eq(hash)
      end
    end

    it { is_expected.to eq('foo=bar') }

    context 'when the value contains an equal sign' do
      let(:hash) { { "foo" => 'barbara=' } }

      it 'puts quotes around the value' do
        expect(subject).to eq('foo="barbara="')
      end
      it_behaves_like 'parsable with logfmt-ruby'
    end

    context 'when the value is a boolean' do
      context 'and it is true' do
        let(:hash) { { "foo" => true } }

        it { is_expected.to eq('foo') }
        it_behaves_like 'parsable with logfmt-ruby'
      end

      context 'and it is false' do
        let(:hash) { { "foo" => false } }

        it { is_expected.to eq('') }
      end
    end

    context 'when the value contains spaced' do
      let(:hash) { { "foo" => 'Hello this is a message' } }

      it 'puts quotes around the value' do
        expect(subject).to eq('foo="Hello this is a message"')
      end
    end

    context 'when the value is empty' do
      let(:hash) { { "foo" => '', "bar" => nil } }

      it { is_expected.to eq('') }
    end
  end
end
