# frozen_string_literal: true

module Logfmt
  class Formatter < Logger::Formatter
    def call(severity, datetime, progname, message)
      message = { message: message } if message.is_a?(String)
      {
        level: severity,
        ts: datetime,
        type: progname
      }.merge(message).to_logfmt + "\n"
    end
  end
end
